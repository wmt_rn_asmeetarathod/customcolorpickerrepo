const Routes = {
    /*  Root Stacks    */

    Authenticated: 'Authenticated',
    UnAuthenticated: 'UnAuthenticated',
    Splash: 'Splash',

    /*  Non-Authenticated Routes    */
    Login: 'Login',

    /*  Authenticated Routes    */

    Home: 'Home',
};
export default Routes;
