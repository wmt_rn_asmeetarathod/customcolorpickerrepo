import React, {
    useState,
    useEffect,
} from 'react';

import {View, Modal, TextInput} from 'react-native';
import {styles} from './styles';
import {ColorPicker} from 'react-native-color-picker';
import {Label} from 'src/component';
import {Color} from 'src/utils';
import PropTypes from 'prop-types';

const CustomColorPicker = (props) =>{

    const [colorInput, setColorInput] = useState(null);
    const [colorPropVisible, setColorPropVisible] = useState(false);
    const [selectedColor, setSelectedColor] = useState();


    /*  Life-cycles Methods */

    useEffect(() => {
        
    }, [selectedColor]);

     /*  UI Events Methods   */
     
    const handleOnCancelPress = () =>{
        setColorPropVisible(false);
        setColorInput('');
        if (props.onCancelPress) {
            props.onCancelPress();
        }
    }

    const handleOnPickPress = () => {
        setColorPropVisible(false);

        let tempColor = '';

        if (colorInput.charAt(0) === "#") {
            setSelectedColor(colorInput);
            tempColor = colorInput;
        } else {
            setSelectedColor(`#${colorInput}`);
            tempColor = `#${colorInput}`;
        }
        if (props.onPickPress) {
            props.onPickPress(tempColor);
        }
        setColorInput('');
    }


    const handleOnColorSelected = (color) => {
        setSelectedColor(color);
        setColorInput(color.substring(1));
        if (props.onColorSelected) {
            props.onColorSelected(color);
        }
    }


    return(
        <View style={
            styles.modalContainer
        }>
            <Modal animationType={props.animationType}
                transparent={props.transparent}
                visible={props.visible}
            >

                <View style={
                    styles.modalUpperView
                }>
                    <View style={
                        styles.modalView
                    }>

                        <View style={
                            [styles.titleStyle,{
                                backgroundColor : props.titleBackgroundColor
                            }]
                        }>
                            <Label large
                                color={
                                    props.titleColor
                            }>
                                {'Pick a Color'}</Label>
                        </View>
                        <View style={
                            styles.bottomView
                        }
                        onResponderMove={
                            ()=>{
                                setColorPropVisible(false)
                            }
                        }
                        onTouchStart={
                            ()=>{
                                setColorPropVisible(false)
                            }
                        }
                        onStartShouldSetResponder={
                            ()=>{
                                setColorPropVisible(false)
                            }
                        }
                        >
                            <View style={
                                [styles.colorPickerViewStyle,{
                                    height : props.colorPickerHeight,
                                    width : props.colorPickerWidth,
                                }]
                            }>
                                <ColorPicker onColorSelected={
                                        color => handleOnColorSelected(color)
                                    }
                                    style={
                                        {flex: 1}
                                    }
                                    color={ colorPropVisible ? colorInput : null}
                                    defaultColor={props.defaultColor}
                                />

                            </View>
                        <View style={
                            styles.vwColorInput
                        }>
                            <View style={
                                [
                                    styles.vwCircle, {
                                        backgroundColor: colorInput ? `#${colorInput}` : Color.BLACK
                                    }
                                ]
                            }/>
                            <View style={
                                styles.inputStyle
                            }>
                                <TextInput defaultValue="#"
                                    style={
                                        styles.colorTextInputStatic
                                    }/>
                                <TextInput style={
                                        [styles.colorTextInput,{ 
                                            color: props.inputTextColor
                                        }]
                                    }
                                    value={colorInput}
                                    onChangeText={
                                        (text) => {
                                            setColorInput(text);
                                        }
                                    }
                                    onFocus={
                                        ()=> {
                                            setColorPropVisible(true)
                                        }
                                    } 
                                    />
                            </View>

                    </View>
                    <View style={
                        styles.vwActionButtons
                    }>
                        <Label style={
                                styles.cancelLblStyle
                            }
                            color={props.cancelLabelColor}
                            onPress={handleOnCancelPress}>
                            {'CANCEL'}</Label>
                        <Label style={
                                styles.pickLblStyle
                            }
                            color={props.pickLabelColor} 
                            onPress={handleOnPickPress}>
                            {'PICK'}</Label>
                    </View>
                </View>


            </View>
        </View>
    </Modal>
</View>
    )
}

CustomColorPicker.defaultProps = {
    ...View.defaultProps,
    animationType: 'slide',
    transparent: false,
    visible: false,
    titleColor: Color.WHITE,
    titleBackgroundColor: "#78BE20",
    cancelLabelColor: Color.BLACK,
    pickLabelColor: Color.BLACK,
    colorPickerHeight: 300,
    colorPickerWidth: 300,
    inputTextColor: Color.BLACK,
    defaultColor : Color.RED,
}

CustomColorPicker.propTypes = {
    ...View.propTypes,
    animationType: PropTypes.string,
    transparent: PropTypes.bool,
    visible: PropTypes.bool,
    titleColor: PropTypes.string,
    titleBackgroundColor: PropTypes.string,
    cancelLabelColor: PropTypes.string,
    pickLabelColor: PropTypes.string,
    colorPickerHeight: PropTypes.number,
    colorPickerWidth: PropTypes.number,
    inputTextColor: PropTypes.string,
    defaultColor : PropTypes.string,
}
export default CustomColorPicker;
