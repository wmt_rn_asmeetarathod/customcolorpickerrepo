import {StyleSheet} from 'react-native';
import {ThemeUtils} from 'src/utils';
import { Color } from '../../../utils';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    titleStyle: {
        height: ThemeUtils.APPBAR_HEIGHT,
        backgroundColor: "#78BE20",
        justifyContent: 'center',
        alignItems: 'center',
        borderTopLeftRadius: 15,
        borderTopRightRadius: 15

    },
    modalContainer: {
        display: 'flex',
        justifyContent: 'center',
        alignContent: 'center',
        alignSelf: 'center',
        flex: 1,
        width: ThemeUtils.relativeWidth(90)
    },
    modalUpperView: {
        display: 'flex',
        justifyContent: 'center',
        alignContent: 'center',
        flex: 1
    },
    modalView: {
        margin: 20,
        backgroundColor: "white",
        borderRadius: 15,
        // padding: 10,
        // alignItems: "center",
        justifyContent: 'center',
        alignSelf: 'center',
        width: ThemeUtils.relativeWidth(90),
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5
    },
    bottomView: {
        padding: 20
    },
    colorPickerViewStyle: {

        height: 300,
        width: 300,
        // alignItems : 'center',
        justifyContent: 'center',
        alignSelf: 'center'


    },
    vwCircle: {
        borderRadius: 50,
        height: 60,
        width: 60,
        // backgroundColor: 'red',
        margin: 20,
        borderWidth : 1,
        borderColor : Color.DARK_LIGHT_BLACK,
    },
    inputStyle:{
        flexDirection : 'row',
        // backgroundColor : 'purple'
    },
    colorTextInputStatic:{
        // backgroundColor : 'pink',
        textAlign : 'right',
        color : Color.DARK_LIGHT_BLACK,
        fontSize: ThemeUtils.fontNormal,
        paddingEnd : 0,
    
    },
    colorTextInput:{
        fontSize: ThemeUtils.fontNormal,
        // backgroundColor : 'yellow',
        textAlign : 'left',
        marginStart :0,
        paddingStart : 0,
        color : Color.BLACK,
    },
    vwColorInput: {
        flexDirection: 'row'
    },
    vwActionButtons: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        
        
    },
    cancelLblStyle: {
        marginEnd: 50

    },
    pickLblStyle: {
        marginEnd: 30
    }
});

