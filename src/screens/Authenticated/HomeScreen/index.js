import React, {useState} from 'react';
import {Alert, View} from 'react-native';
import {styles} from './styles';
import {CommonStyle, ThemeUtils} from 'src/utils';
import {RoundButton, Label, CustomColorPicker} from 'src/component';
import {Color} from 'src/utils';


const Home = (props) => {

    const [visible, setVisible] = useState(false);


    /*  Public Interface Methods */

    /*  Validation Methods  */

    /*  UI Events Methods   */

    const handleOnPress = () => {
        setVisible(true);
    };

    const handleOnPickPress = (selectedColor) => {
        setVisible(false);
        console.log("Home handleOnPickPress selectedColor ==>> ", selectedColor);
        Alert.alert("Color",`Selected Color is ${selectedColor}`);
    }

    const handleOnCancelPress = () => {
        setVisible(false);
    }
    /*  Custom-Component sub-render Methods */

    return (
        <View style={
            CommonStyle.master_full_flex
        }>
            {
            visible && <CustomColorPicker animationType="slide"
                transparent={false}
                visible={visible}
                titleBackgroundColor={
                    Color.LIME
                }
                titleColor={
                    Color.WHITE
                }              
                cancelLabelColor={
                    Color.LIME
                }
                pickLabelColor={
                    Color.LIME
                }
                defaultColor={Color.RED}
                onCancelPress={handleOnCancelPress}
                onPickPress={handleOnPickPress}/>
        }
            <View style={
                styles.container
            }>
                <Label>{'Home Screen'}</Label>
                <RoundButton click={handleOnPress}
                    width={
                        ThemeUtils.relativeRealWidth(90)
                    }
                    mt={20}>
                    {'Select Color'}</RoundButton>
                    


            </View>

        </View>
    );
};

export default Home;
